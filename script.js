let switchMode = document.getElementById('switchMode');

function changeTheme() {
    let theme = document.getElementById("theme");
    if(theme.getAttribute('href') == './CSS/dark-mode.css'){
        localStorage.setItem('theme', 'light');
        theme.href = './CSS/light-mode.css';        
    }else {
        localStorage.setItem('theme', 'dark');
        theme.href = './CSS/dark-mode.css'        
    }
}

switchMode.addEventListener('click', changeTheme);

if(localStorage.getItem('theme') === 'dark'){
    theme.href = './CSS/dark-mode.css'
} else {
    theme.href = './CSS/light-mode.css'
}
